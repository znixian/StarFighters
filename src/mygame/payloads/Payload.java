/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.payloads;

import com.jme3.bullet.collision.PhysicsCollisionEvent;
import mygame.controls.MissileControl;

/**
 *
 * @author znix
 */
public interface Payload {

    void detonate(MissileControl control);

    default void collide(MissileControl control, PhysicsCollisionEvent event) {
        detonate(control);
        control.destroy();
    }

    float getMaxDetonateRange();
}
