/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.payloads;

import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.effect.ParticleEmitter;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import mygame.controls.MissileControl;
import mygame.states.IngameAppstate;

/**
 *
 * @author znix
 */
public class KineticPayload implements Payload {

    @Override
    public void detonate(MissileControl control) {
        Vector3f velocity = control.getVelocity();

        Vector3f trgPos = control.getTarget().getWorldTranslation();
        Vector3f ourPos = control.getLocation();
        Vector3f diff = trgPos.subtract(ourPos);
        diff.normalizeLocal();
        diff.multLocal(velocity.length());
        control.getBody().setLinearVelocity(diff);
    }

    @Override
    public void collide(MissileControl control, PhysicsCollisionEvent event) {
        Spatial obj = control.getSpatial();
        Node crafts = control.getSpatial().getParent();
        Node debris = (Node) crafts.getParent().getChild(IngameAppstate.DEBRIS_NODE_NAME);

//        control.disablePhysics(); // want it to keep spinning.
        obj.removeControl(control);
        obj.removeFromParent();
        debris.attachChild(obj);

        if (obj instanceof Node) {
            Node n = (Node) obj;
            Spatial trail = n.getChild("EngineTrail");
            if (trail != null) {
//            trail.removeFromParent();
                ((ParticleEmitter) trail).setParticlesPerSec(0); // kill the engine
            }
        }
    }

    @Override
    public float getMaxDetonateRange() {
        return 15;
    }
}
