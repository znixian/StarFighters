/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.payloads;

import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import mygame.controls.MissileControl;

/**
 *
 * @author znix
 */
public class ExplosivePayload implements Payload {

    @Override
    public void detonate(MissileControl control) {
        Spatial target = control.getTarget();

        RigidBodyControl body = target.getControl(RigidBodyControl.class);
        Vector3f enemyPos = body.getPhysicsLocation();
        enemyPos.subtractLocal(control.getLocation());
        enemyPos.normalizeLocal();
        enemyPos.multLocal(100);
        body.applyImpulse(enemyPos, Vector3f.ZERO);

        deactivate(control);
    }

    private void deactivate(MissileControl control) {
        control.getMain().getEffects().explode(control.getSpatial().getWorldTranslation());
        control.destroy();
        control.getSpatial().removeControl(control);
    }

    @Override
    public float getMaxDetonateRange() {
        return 20;
    }

    @Override
    public void collide(MissileControl control, PhysicsCollisionEvent event) {
        Spatial target = control.getTarget();
        if (target == null) {
            return;
        }
        RigidBodyControl body = target.getControl(RigidBodyControl.class);
        if (event.getObjectA() == body || event.getObjectB() == body) {
            detonate(control);
        } else {
            deactivate(control);
        }
    }

}
