/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mygame.weapons;

import com.jme3.bullet.PhysicsSpace;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import mygame.Main;
import mygame.controls.CraftControl;
import mygame.controls.MissileControl;
import mygame.payloads.Payload;
import mygame.states.IngameAppstate;

/**
 *
 * @author znix
 */
public abstract class BaseWeaponMissile implements Weapon {

    @Override
    public void fire(CraftControl craft) {
        // interesting variables
        Spatial target = craft.getCurrentTarget();
        Payload payload = buildPayload();

        // boring variables
        Node crafts = craft.getSpatial().getParent();
        IngameAppstate main = craft.getMain();
        PhysicsSpace physics = craft.getPhysics();
        Quaternion rotationOffset = craft.getRotationOffset();
        Quaternion rotation = craft.getRotation();

        Spatial missile = MissileControl.addMissile(
                main, physics,
                payload, craft, rotationOffset,
                rotation.mult(new Vector3f(0, -2.5f, 0)));
        crafts.attachChild(missile);

        missile.getControl(MissileControl.class).setTarget(target);
    }

    protected abstract Payload buildPayload();
}
