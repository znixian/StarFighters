/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mygame.weapons;

import mygame.payloads.ExplosivePayload;
import mygame.payloads.KineticPayload;
import mygame.payloads.Payload;

/**
 *
 * @author znix
 */
public class WeaponMissileSingleShot extends BaseWeaponMissile {

    private final MissileType type;

    public WeaponMissileSingleShot(MissileType type) {
        this.type = type;
    }

    @Override
    protected Payload buildPayload() {
        switch (type) {
            case EXPLOSIVE:
                return new ExplosivePayload();
            case KINETIC:
                return new KineticPayload();
            default:
                throw new IllegalStateException("Bad type " + type);
        }
    }

    @Override
    public float getShotCooldown() {
        return 5;
    }

    @Override
    public float getReloadCooldown() {
        return 0;
    }

    @Override
    public int getChainLoads() {
        return 1;
    }

    public enum MissileType {
        KINETIC, EXPLOSIVE;
    }

}
