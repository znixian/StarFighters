/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mygame.weapons;

import mygame.Main;
import mygame.controls.CraftControl;

/**
 *
 * @author znix
 */
public interface Weapon {

    /**
     * Get the cooldown, per shot, of the weapon. After the weapon is fired,
     * this time must elapse before it can be fired again.
     *
     * @return The cooldown in seconds.
     */
    float getShotCooldown();

    /**
     * Get the cooldown, per reload, of the weapon. This is the time it takes to
     * regenerate 1 unit of ammo, out of the amount specified in
     * {@link #getChainLoads() getChainLoads()}.
     *
     * @return The cooldown in seconds.
     */
    float getReloadCooldown();

    /**
     * Get the number of shots that may be fired without the reload cooldown
     * elapsing.
     *
     * @return The number of shots that may be held.
     */
    int getChainLoads();

    /**
     * Physically fire this weapon, spawning a missile or whatever.
     *
     * @param main The game.
     * @param craft The craft that is shooting.
     */
    void fire(CraftControl craft);
}
