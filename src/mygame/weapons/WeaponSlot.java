/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mygame.weapons;

import mygame.Main;
import mygame.controls.CraftControl;

/**
 *
 * @author znix
 */
public class WeaponSlot {

    private final Weapon weapon;
    private float shotCooldownTimer;
    private float reloadCooldownTimer;
    private int roundsHeld;

    public WeaponSlot(Weapon weapon) {
        this.weapon = weapon;
    }

    public void update(float tpf) {
        boolean chainFull = roundsHeld >= weapon.getChainLoads();

        shotCooldownTimer -= shotCooldownTimer < 0 ? 0 : tpf;
        reloadCooldownTimer -= chainFull ? 0 : tpf;

        if (reloadCooldownTimer < 0 && !chainFull) {
            reloadCooldownTimer = weapon.getReloadCooldown();
            roundsHeld++;
        }
    }

    public Weapon getWeapon() {
        return weapon;
    }

    /**
     * Does this weapon have rounds ready to be used
     *
     * @return True if there are rounds to be fired, false otherwise.
     */
    public boolean hasRounds() {
        return roundsHeld > 0;
    }

    /**
     * Is the cooldown timer for each shot elapsed?
     *
     * @return True if the shooting cooldown is done.
     */
    public boolean isPerShotTimerElapsed() {
        return shotCooldownTimer <= 0;
    }

    /**
     * Is this weapon ready to be fired?
     *
     * @return True if this weapon can be fired, false otherwise.
     */
    public boolean isReady() {
        return hasRounds() && isPerShotTimerElapsed();
    }

    /**
     * Call this if you manually fire the weapon, as it resets the cooldowns and
     * round count.
     */
    public void onFired() {
        roundsHeld--;
        shotCooldownTimer = weapon.getShotCooldown();
    }

    /**
     * Actually fire this weapon. Note that the craft to fire this from does not
     * necessarally have to have this WeaponSlot installed. Eg, you can have
     * some kind of teleporting missile that launches from other friendly ships.
     *
     * @param craft The craft to fire the weapon from.
     */
    public void fire(CraftControl craft) {
        onFired();
        weapon.fire(craft);
    }
}
