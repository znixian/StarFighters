/*
 * Copyright (C) 2016 znix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mygame.states;

import com.jme3.app.Application;
import static com.jme3.app.SimpleApplication.INPUT_MAPPING_EXIT;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.PhysicsTickListener;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.Spatial.CullHint;
import com.jme3.scene.shape.Quad;
import com.jme3.scene.shape.Sphere;
import com.jme3.system.AppSettings;
import com.jme3.util.SkyFactory;
import mygame.GFX;
import mygame.Main;
import mygame.controls.AICraftControl;
import mygame.controls.PhysicalObjectControl;
import mygame.controls.PlayerCraftControl;
import mygame.gui.OrientationIndicator;

/**
 *
 * @author znix
 */
public class IngameAppstate extends AbstractAppState {

    private Main main;
    private AssetManager assetManager;
    private Camera cam;
    private InputManager inputManager;
    private AppSettings settings;

    private Node rootNode;
    private Node guiNode;

    private BulletAppState bulletAppState;
    private Spatial playerCraft;
    private PlayerCraftControl playerControl;
    private Node crafts;
    private Node debris;
    private OrientationIndicator indicator;
    private GFX effects;

    public static final String DEBRIS_NODE_NAME = "debris";

    @Override
    public void update(float tpf) {
        Quaternion rotation = playerCraft.getWorldRotation();
//        Vector3f camOffset = rotation.mult(new Vector3f(5, 0, 0));
        Vector3f camOffset = rotation.mult(new Vector3f(-25, 3, 0));
        cam.setLocation(playerCraft.getWorldTranslation().add(camOffset));

        Quaternion camLook = new Quaternion();
        Vector3f camLookDir = rotation.mult(new Vector3f(1, 0, 0));
        Vector3f camLookUp = rotation.mult(new Vector3f(0, 1, 0));
        camLook.lookAt(camLookDir, camLookUp);
        cam.setRotation(camLook);

        indicator.update(playerControl, tpf, crafts);
    }

    @Override
    public void stateDetached(AppStateManager stateManager) {
    }

    @Override
    public void stateAttached(AppStateManager stateManager) {
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);

        bulletAppState.getPhysicsSpace().setGravity(Vector3f.ZERO);

        effects = new GFX(this);

        crafts = new Node();
        rootNode.attachChild(crafts);
        rootNode.attachChild(debris = new Node(DEBRIS_NODE_NAME));

        playerCraft = PlayerCraftControl.addCraft(this, bulletAppState.getPhysicsSpace());
        playerCraft.getControl(RigidBodyControl.class).setPhysicsLocation(new Vector3f(0, 5, 0));
        playerControl = playerCraft.getControl(PlayerCraftControl.class);
        crafts.attachChild(playerCraft);

        Spatial ai1 = AICraftControl.addAICraft(this, bulletAppState.getPhysicsSpace(), playerControl);
        ai1.getControl(RigidBodyControl.class).setPhysicsLocation(new Vector3f(200, 0, 0));
        crafts.attachChild(ai1);

        AmbientLight al = new AmbientLight();
        al.setColor(ColorRGBA.White.mult(1.3f));
        rootNode.addLight(al);

        PhysicsListener physicsListener = new PhysicsListener();
        bulletAppState.getPhysicsSpace().addTickListener(physicsListener);
        bulletAppState.getPhysicsSpace().addCollisionListener(physicsListener);

        initControls();
        initHUD();
        initAsteroids();
//        initSkybox();
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        main = (Main) app;
        assetManager = main.getAssetManager();
        cam = main.getCamera();
        inputManager = main.getInputManager();
        settings = main.getSettings();

        rootNode = new Node("IngameAppState-world");
        guiNode = new Node("IngameAppState-gui");

        main.getRootNode().attachChild(rootNode);
        main.getGuiNode().attachChild(guiNode);

        System.out.println("mygame.states.IngameAppstate.initialize()");
    }

    private void initControls() {
        final String fwd = "Forwards";
        final String bck = "Backwards";
        final String lft = "Left";
        final String rgt = "Right";
        final String hor = "Roll";
        final String vtc = "Pitch";
        final String rol = "RollLeft";
        final String ror = "RollRight";
        final String vup = "Up";
        final String vdw = "Down";
        final String sht = "Shoot";
        final String trk = "Track";
        final String tgt = "Target";
        final String horI = hor + "I";
        final String vtcI = vtc + "I";

        inputManager.clearMappings();
        inputManager.addMapping(INPUT_MAPPING_EXIT, new KeyTrigger(KeyInput.KEY_ESCAPE));
        inputManager.addMapping(fwd, new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping(bck, new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping(lft, new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping(rgt, new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping(rol, new KeyTrigger(KeyInput.KEY_Q));
        inputManager.addMapping(ror, new KeyTrigger(KeyInput.KEY_E));
        inputManager.addMapping(vup, new KeyTrigger(KeyInput.KEY_LSHIFT));
        inputManager.addMapping(vdw, new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping(trk, new KeyTrigger(KeyInput.KEY_F));

        inputManager.addMapping(hor, new MouseAxisTrigger(MouseInput.AXIS_X, false));
        inputManager.addMapping(vtc, new MouseAxisTrigger(MouseInput.AXIS_Y, false));
        inputManager.addMapping(horI, new MouseAxisTrigger(MouseInput.AXIS_X, true));
        inputManager.addMapping(vtcI, new MouseAxisTrigger(MouseInput.AXIS_Y, true));

        inputManager.addMapping(sht, new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addMapping(tgt, new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));

        inputManager.addListener((ActionListener) (name, isPressed, tpf) -> {
            main.stop();
        }, INPUT_MAPPING_EXIT);

        inputManager.addListener((ActionListener) (name, isPressed, tpf) -> {
            switch (name) {
                case fwd:
                    playerControl.setFwd(isPressed);
                    break;
                case bck:
                    playerControl.setBck(isPressed);
                    break;
                case lft:
                    playerControl.setLft(isPressed);
                    break;
                case rgt:
                    playerControl.setRgt(isPressed);
                    break;
                case rol:
                    playerControl.setRol(isPressed);
                    break;
                case ror:
                    playerControl.setRor(isPressed);
                    break;
                case vup:
                    playerControl.setVup(isPressed);
                    break;
                case vdw:
                    playerControl.setVdw(isPressed);
                    break;
                case sht:
                    if (isPressed) {
                        playerControl.shoot();
                    }
                    break;
                case trk:
                    if (isPressed) {
                        playerControl.setTrk(!playerControl.isTrk());
                    }
                    break;
                case tgt:
                    if (isPressed) {
                        playerControl.target();
                    }
                    break;
            }
        }, fwd, bck, lft, rgt, rol, ror, vup, vdw, sht, trk, tgt);

        inputManager.addListener((AnalogListener) (name, value, tpf) -> {
            switch (name) {
                case hor:
                    playerControl.addHor(value);
                    break;
                case vtc:
                    playerControl.addVtc(value);
                    break;
                case horI:
                    playerControl.addHor(-value);
                    break;
                case vtcI:
                    playerControl.addVtc(-value);
                    break;
            }
        }, hor, vtc, horI, vtcI);
    }

    private void initHUD() {
        int hudHeight = settings.getHeight();
        int hudWidth = hudHeight / OrientationIndicator.HEIGHT_RATIO;

        float hudAreaWidth = 1f * hudWidth / settings.getWidth();
        float mainViewWidth = 1 - hudAreaWidth;

        cam.setViewPort(0, mainViewWidth, 0, 1);

        int hudAreaStart = (int) (settings.getWidth() * mainViewWidth);

        Quad b = new Quad(hudWidth, hudHeight);
        Geometry geom = new Geometry("Box", b);
        Material mat = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Blue);
        geom.setMaterial(mat);
        geom.setLocalTranslation(hudAreaStart, 0, 0);
        guiNode.attachChild(geom);

        indicator = new OrientationIndicator(this, hudWidth);
        indicator.setLocalTranslation(hudAreaStart + hudWidth / 2, hudHeight - hudWidth / 2, 0);
        guiNode.attachChild(indicator);

        initCrossHairs(mainViewWidth);
    }

    public AppSettings getSettings() {
        return settings;
    }

    public GFX getEffects() {
        return effects;
    }

    private void initAsteroids() {
        Sphere obj = new Sphere(7, 7, 25);
        Material mat = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Yellow);

        Vector3f pos = new Vector3f();

        for (int i = 0; i < 250; i++) {
            Geometry geom = new Geometry("Planet", obj);
            geom.setMaterial(mat);
            pos.set(
                    (float) Math.random(),
                    (float) Math.random(),
                    (float) Math.random());
            pos.subtractLocal(0.5f, 0.5f, 0.5f);
            pos.multLocal(5000);
            geom.setLocalTranslation(pos);
//            geom.setLocalScale((float) Math.random() + 0.25f);
            geom.addControl(new RigidBodyControl(0));
            bulletAppState.getPhysicsSpace().add(geom);
            rootNode.attachChild(geom);
        }
    }

    protected void initCrossHairs(float width) {
        BitmapFont guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        BitmapText ch = new BitmapText(guiFont, false);
        ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
        ch.setText("+"); // crosshairs
        ch.setLocalTranslation( // center
                settings.getWidth() / 2 * width - guiFont.getCharSet().getRenderedSize() / 3 * 2,
                settings.getHeight() / 2 + ch.getLineHeight() / 2, 0);
        guiNode.attachChild(ch);
    }

    private void initSkybox() {
        Spatial skygeom = SkyFactory.createSky(assetManager, "Textures/Sky.dds", SkyFactory.EnvMapType.CubeMap);
        skygeom.setQueueBucket(Bucket.Sky);
        skygeom.setLocalScale(500);
        skygeom.setCullHint(CullHint.Never);
        rootNode.attachChild(skygeom);
    }

    private class PhysicsListener implements PhysicsTickListener, PhysicsCollisionListener {

        @Override
        public void prePhysicsTick(PhysicsSpace space, float tpf) {
            crafts.getChildren().stream()
                    .map((spatial) -> spatial.getControl(PhysicalObjectControl.class))
                    .forEach((control) -> {
                        control.prePhysicsTick(space, tpf);
                    });
        }

        @Override
        public void physicsTick(PhysicsSpace space, float tpf) {
            crafts.getChildren().stream()
                    .map((spatial) -> spatial.getControl(PhysicalObjectControl.class))
                    .forEach((control) -> {
                        control.physicsTick(space, tpf);
                    });
        }

        @Override
        public void collision(PhysicsCollisionEvent event) {
            crafts.getChildren().stream()
                    .map((spatial) -> spatial.getControl(PhysicalObjectControl.class))
                    .forEach((control) -> {
                        control.collisionAny(event);
                    });
        }

    }

    public Main getMain() {
        return main;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public Node getRootNode() {
        return rootNode;
    }
}
