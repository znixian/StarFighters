/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.gui;

import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer;
import com.jme3.texture.Texture;
import com.jme3.util.BufferUtils;
import mygame.controls.PlayerCraftControl;
import mygame.states.IngameAppstate;

/**
 *
 * @author znix
 */
public class OrientationIndicator extends Node {

    public static final int HEIGHT_RATIO = 3;
    private Spatial ball;
    private Vector2f[] texCoord;
    private Mesh mesh;
    private final Vector3f mapPos = new Vector3f();
    private float timer;
    private Radar radar;

    public OrientationIndicator(IngameAppstate main, int width) {
        initNavBall(main, width);
        initNavGrid(main, width);
        initRadar(main, width);
    }

    private void initNavBall(IngameAppstate main, int width) {
        ball = main.getAssetManager().loadModel("Models/OriBall.j3o");
        ball.setLocalScale(width / 2);
        attachChild(ball);

        // You must add a light to make the model visible
        DirectionalLight lamp = new DirectionalLight(new Vector3f(0, 0, -1));
        addLight(lamp);
    }

    private void initNavGrid(IngameAppstate main, int width) {
        mesh = new Mesh();

        // Vertex positions in space
        Vector3f[] vertices = new Vector3f[8];
        float border = -0.75f;
        vertices[0] = new Vector3f(border, -1, 0);
        vertices[1] = new Vector3f(1, -1, 0);
        vertices[2] = new Vector3f(border, 1, 0);
        vertices[3] = new Vector3f(1, 1, 0);

        vertices[4] = new Vector3f(-1, -1, 0);
        vertices[5] = new Vector3f(border, -1, 0);
        vertices[6] = new Vector3f(-1, 1, 0);
        vertices[7] = new Vector3f(border, 1, 0);

        // Texture coordinates
        texCoord = new Vector2f[8];
        texCoord[0] = new Vector2f(0, 0);
        texCoord[1] = new Vector2f(1, 0);
        texCoord[2] = new Vector2f(0, 1);
        texCoord[3] = new Vector2f(1, 1);

        texCoord[4] = new Vector2f();
        texCoord[5] = new Vector2f();
        texCoord[6] = new Vector2f();
        texCoord[7] = new Vector2f();

        // Indexes. We define the order in which mesh should be constructed
        short[] indexes = {2, 0, 1, 1, 3, 2,
            6, 4, 5, 5, 7, 6};

        // Setting buffers
        mesh.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
        mesh.setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoord));
        mesh.setBuffer(VertexBuffer.Type.Index, 1, BufferUtils.createShortBuffer(indexes));
        mesh.updateBound();

        // *************************************************************************
        // First mesh uses one solid color
        // *************************************************************************
        // Creating a geometry, and apply a single color material to it
        Geometry geom = new Geometry("OurMesh", mesh);
        Material mat = new Material(main.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
//        mat.setColor("Color", ColorRGBA.Gray);
        Texture tex = main.getAssetManager().loadTexture("Textures/MovementTrackerGrid.png");
        tex.setMinFilter(Texture.MinFilter.NearestNoMipMaps);
        mat.setTexture("ColorMap", tex);
        geom.setMaterial(mat);
        geom.setLocalTranslation(0, -width, 0);
        geom.setLocalScale(width / 2);
        attachChild(geom);
    }

    private void initRadar(IngameAppstate main, int width) {
        radar = new Radar(main, width);
        radar.setLocalTranslation(-width / 2, -width * 2.5f, 0);
        attachChild(radar);
    }

    public void update(PlayerCraftControl craft, float tpf, Node crafts) {
        Quaternion rotation = craft.getRotation();
        Quaternion undo = rotation.inverse();
        Vector3f velocity = craft.getVelocity();

        updateOriBall(rotation, velocity, tpf);
        updateNavGrid(rotation, velocity, tpf, undo);
        radar.update(tpf, crafts, craft.getSpatial(), undo);
    }

    private void updateOriBall(Quaternion rotation, Vector3f velocity, float tpf) {
        ball.setLocalRotation(rotation);
        Quaternion rot = ball.getLocalRotation();
        float x = rotation.getX();
        float y = rotation.getY();
        float z = rotation.getZ();
        float w = rotation.getW();
        rot.set(z, y, x, w);
    }

    private void updateNavGrid(Quaternion rotation, Vector3f velocity, float tpf, Quaternion undo) {
        if (undo != null) {
            undo.mult(velocity, velocity);
        }
        float imgMoveFactor = 0.01f;
        float mult = imgMoveFactor * tpf;

        mapPos.x += velocity.z * mult;
        mapPos.y += velocity.x * mult;
        mapPos.z += velocity.y * mult;

        float half = 0.25f;
        float vBarW = 64f / 512f;
        float vBarEnd = 1;
        float vBarStart = vBarEnd - vBarW;
        if (mapPos.x > half) {
            mapPos.x -= half;
        }
        if (mapPos.y > half) {
            mapPos.y -= half;
        }
        if (mapPos.x < 0) {
            mapPos.x += half;
        }
        if (mapPos.y < 0) {
            mapPos.y += half;
        }
        if (mapPos.z < 0) {
            mapPos.z += half;
        }
        if (mapPos.z > half) {
            mapPos.z -= half;
        }

        texCoord[0].set(mapPos.x + 0.0f, mapPos.y + 0.0f);
        texCoord[1].set(mapPos.x + half, mapPos.y + 0.0f);
        texCoord[2].set(mapPos.x + 0.0f, mapPos.y + half);
        texCoord[3].set(mapPos.x + half, mapPos.y + half);

        texCoord[4].set(vBarStart, mapPos.z + 0.0f);
        texCoord[5].set(vBarEnd, mapPos.z + 0.0f);
        texCoord[6].set(vBarStart, mapPos.z + half);
        texCoord[7].set(vBarEnd, mapPos.z + half);

        mesh.setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoord));
        mesh.updateBound();
    }
}
