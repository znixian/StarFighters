/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.gui;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Texture;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import mygame.states.IngameAppstate;

/**
 *
 * @author znix
 */
public class Radar extends Node {

    public static final int HEIGHT = 20;
    private final ArrayList<Marker> markerPoolAbove = new ArrayList<>();
    private final ArrayList<Marker> markerPoolBelow = new ArrayList<>();
    private final ArrayList<Marker> markerPoolFriendly = new ArrayList<>();
    private final Material markerMatAbove;
    private final Material markerMatBelow;
    private final Material markerMatFriendly;
    private final Quad marker;
    private final int width;
    private final Map<Spatial, Marker> markers = new HashMap<>();
    private final Vector3f tmp = new Vector3f();
    private final Vector3f tmp2 = new Vector3f();
    private final List<Spatial> tmpL = new ArrayList<>();

    public Radar(IngameAppstate main, int width) {
        this.width = width;
        int itemSize = width / HEIGHT;
        AssetManager assetManager = main.getAssetManager();
        Quad b = new Quad(width, width);
        Geometry geom = new Geometry("Box", b);
        Material mat = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Green);
        geom.setMaterial(mat);
        attachChild(geom);

        marker = new Quad(itemSize, itemSize);
        markerMatAbove = makeMat(assetManager, "Textures/RadarUp.png");
        markerMatBelow = makeMat(assetManager, "Textures/RadarDown.png");
        markerMatFriendly = makeMat(assetManager, "Textures/RadarFriendly.png");
        Marker mark = makeMarker(CraftType.FRIENDLY);
        mark.attach();
        mark.setPosition(width / 2, width / 2);
    }

    private Material makeMat(AssetManager assetManager, String texture) {
        Material mat = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        Texture tex2 = assetManager.loadTexture(texture);
        tex2.setMinFilter(Texture.MinFilter.NearestNoMipMaps);
        tex2.setMagFilter(Texture.MagFilter.Nearest);
        mat.setTexture("ColorMap", tex2);
        mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);

        return mat;
    }

    private Marker makeMarker(CraftType type) {
        ArrayList<Marker> pool = type.getPool(this);
        if (pool.isEmpty()) {
            Geometry geom = new Geometry("Marker", marker);
            geom.setMaterial(type.getMaterial(this));
//            geom.setQueueBucket(Bucket.Transparent);
            return new Marker(geom, type);
        }
        return pool.remove(pool.size() - 1);
    }

    public void update(float tpf, Node crafts, Spatial ourCraft, Quaternion undoCraftRotation) {
        Vector3f us = ourCraft.getLocalTranslation();
        tmpL.clear();
        tmpL.add(ourCraft);
        for (Spatial craft : crafts.getChildren()) {
            if (craft == ourCraft) {
                continue;
            }
            tmpL.add(craft);
            tmp.set(craft.getLocalTranslation());
            tmp.subtractLocal(us);
            undoCraftRotation.mult(tmp, tmp);
            tmp.multLocal(0.05f);
            boolean above = tmp.y > 0;
            CraftType type = above ? CraftType.ABOVE : CraftType.BELOW;

            Marker mark = markers.get(craft);
            if (mark == null) {
                mark = makeMarker(type);
                mark.attach();
                markers.put(craft, mark);
            } else if (mark.getType() != type) {
                mark.detach();
                mark = makeMarker(type);
                mark.attach();
                markers.put(craft, mark);
            }
            tmp2.set(tmp.z, tmp.x, 0);
            tmp2.addLocal(width / 2, width / 2, 0);
            mark.setPosition(tmp2);
        }

        for (Iterator<Map.Entry<Spatial, Marker>> ite = markers.entrySet().iterator(); ite.hasNext();) {
            Map.Entry<Spatial, Marker> item = ite.next();
            if (!tmpL.contains(item.getKey())) {
                item.getValue().detach();
                ite.remove();
            }
        }
    }

    private enum CraftType {
        ABOVE {
            @Override
            public ArrayList<Marker> getPool(Radar radar) {
                return radar.markerPoolAbove;
            }

            @Override
            public Material getMaterial(Radar radar) {
                return radar.markerMatAbove;
            }
        }, BELOW {
            @Override
            public ArrayList<Marker> getPool(Radar radar) {
                return radar.markerPoolBelow;
            }

            @Override
            public Material getMaterial(Radar radar) {
                return radar.markerMatBelow;
            }
        }, FRIENDLY {
            @Override
            public ArrayList<Marker> getPool(Radar radar) {
                return radar.markerPoolFriendly;
            }

            @Override
            public Material getMaterial(Radar radar) {
                return radar.markerMatFriendly;
            }
        };

        public abstract ArrayList<Marker> getPool(Radar radar);
        public abstract Material getMaterial(Radar radar);
    }

    private class Marker {

        private final Geometry indicator;
        private final CraftType type;

        public Marker(Geometry indicator, CraftType type) {
            this.indicator = indicator;
            this.type = type;
        }

        public void attach() {
            attachChild(indicator);
        }

        public void detach() {
            detachChild(indicator);
            type.getPool(Radar.this).add(this);
        }

        public CraftType getType() {
            return type;
        }

        public void setPosition(Vector3f pos) {
            setPosition(pos.x, pos.y);
        }

        public void setPosition(float x, float y) {
            indicator.setLocalTranslation(x, y, 0);
        }
    }
}
