/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.effect.shapes.EmitterSphereShape;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import mygame.states.IngameAppstate;

/**
 *
 * @author znix
 */
public class GFX {

    private final IngameAppstate parent;
    private final Node gfxNode;
    private final Material mat;

    public GFX(IngameAppstate parent) {
        this.parent = parent;
        gfxNode = new Node("GFX");
        parent.getRootNode().attachChild(gfxNode);
        AssetManager assetManager = parent.getAssetManager();

        mat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/flame.png"));
    }

    public void explode(Vector3f pos) {
        int COUNT_FACTOR = 4;
        float COUNT_FACTOR_F = 1f;
        ParticleEmitter effect = new ParticleEmitter("Flame", ParticleMesh.Type.Triangle, 32 * COUNT_FACTOR);
        effect.setSelectRandomImage(true);
        effect.setStartColor(new ColorRGBA(1f, 0.4f, 0.05f, (float) (1f / COUNT_FACTOR_F)));
        effect.setEndColor(new ColorRGBA(.4f, .22f, .12f, 0f));
        effect.setStartSize(1.3f);
        effect.setEndSize(2f);
        effect.setShape(new EmitterSphereShape(Vector3f.ZERO, 1f));
        effect.setParticlesPerSec(0);
        effect.setGravity(0, -5f, 0);
        effect.setLowLife(.4f);
        effect.setHighLife(1.5f);
        effect.setInitialVelocity(new Vector3f(0, 7, 0));
        effect.setVelocityVariation(1f);
        effect.setImagesX(2);
        effect.setImagesY(2);
        effect.setMaterial(mat);

        effect.setLocalTranslation(pos);
        gfxNode.attachChild(effect);
        effect.emitAllParticles();
    }
}
