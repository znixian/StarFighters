/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.controls;

import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import mygame.states.IngameAppstate;
import mygame.weapons.WeaponMissileSingleShot;
import mygame.weapons.WeaponSlot;

/**
 *
 * @author znix
 */
public class AICraftControl extends CraftControl {

    public static final float MIN_CORRECTION_DIST = 0.1f;

    private float reloadTimer;
    private boolean timeToShoot;
    private float distanceSqLast;

    private final CraftControl target;
    private final Vector3f t_headingToEnemy = new Vector3f();
    private final Vector3f t_prograde = new Vector3f();
//    private final Vector3f t_idealHeading = new Vector3f();
    private final Vector3f t_targetFacing = new Vector3f();
    private final Quaternion t_facingNowQ = new Quaternion();
    private final Vector3f t_engineThrust = new Vector3f();
    private final Quaternion t_dir = new Quaternion();
    private final Vector3f t_enemyPos = new Vector3f();

    public static Spatial addAICraft(IngameAppstate main, PhysicsSpace physics, CraftControl target) {
        Spatial craft = main.getAssetManager().loadModel("Models/Craft1BP.j3o");
        RigidBodyControl body;
        craft.addControl(body = new RigidBodyControl(2));
        craft.addControl(new AICraftControl(main, physics, target));

        body.setDamping(0.1f, 0.1f);

        physics.add(craft);
        return craft;
    }

    public AICraftControl(IngameAppstate main, PhysicsSpace physics, CraftControl target) {
        super(main, physics);
        this.target = target;

        weapons = new WeaponSlot[]{
            new WeaponSlot(new WeaponMissileSingleShot(WeaponMissileSingleShot.MissileType.EXPLOSIVE)),
            new WeaponSlot(new WeaponMissileSingleShot(WeaponMissileSingleShot.MissileType.EXPLOSIVE))
        };
    }

    @Override
    protected void controlUpdate(float tpf) {
        super.controlUpdate(tpf);

        reloadTimer += tpf;
        if (reloadTimer > 0.5 && timeToShoot) {
            reloadTimer = 0;
            WeaponSlot weapon = getFirstReadyWeapon();
            if (weapon != null) {
                weapon.fire(this);
            }
        }
    }

    @Override
    public void prePhysicsTick(PhysicsSpace space, float tpf) {
        float idealDistAway = 150;
        float idealDistAwaySq = idealDistAway * idealDistAway;

        Vector3f position = getLocation();
        body.getPhysicsRotation(t_facingNowQ);
        t_enemyPos.set(target.getLocation());
        t_enemyPos.subtract(position, t_headingToEnemy);
        float distSq = t_headingToEnemy.lengthSquared();
        t_headingToEnemy.normalizeLocal();

        body.getLinearVelocity(t_prograde);
        t_prograde.normalizeLocal();

        if (distSq > idealDistAwaySq) {
            t_targetFacing.set(t_headingToEnemy);
            timeToShoot = false;
        } else {
            t_targetFacing.set(t_headingToEnemy);
            t_targetFacing.negateLocal();
            timeToShoot = distanceSqLast > distSq;
        }
        distanceSqLast = distSq;

        t_dir.lookAt(t_targetFacing, Vector3f.UNIT_Y);
        t_dir.multLocal(0, -1, 0, 1);
        t_facingNowQ.slerp(t_dir, tpf * 2);
        body.setPhysicsRotation(t_facingNowQ);

        float thrust = 2000;

        t_facingNowQ.mult(Vector3f.UNIT_X, t_engineThrust);
        t_engineThrust.normalizeLocal();
        t_engineThrust.multLocal(tpf * thrust * body.getMass());
        body.applyCentralForce(t_engineThrust);
    }

    @Override
    public Spatial getCurrentTarget() {
        return target.getSpatial();
    }
}
