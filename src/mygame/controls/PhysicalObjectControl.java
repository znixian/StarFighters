/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.controls;

import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.BillboardControl;
import com.jme3.scene.shape.Quad;
import mygame.states.IngameAppstate;

/**
 *
 * @author znix
 */
public class PhysicalObjectControl extends AbstractControl {

    public static final String TAG_NAME = "CraftControl-Tag";

    protected final Vector3f acceleration = new Vector3f();
    protected RigidBodyControl body;
    protected final PhysicsSpace physics;
    protected final IngameAppstate main;
    private final Quaternion rotation = new Quaternion();
    private boolean showOutline = true;

    private static Material markerMat;

    public PhysicalObjectControl(IngameAppstate main, PhysicsSpace physics) {
        this.main = main;
        this.physics = physics;
    }

    @Override
    public void setSpatial(Spatial spatial) {
        if (this.spatial != null && showOutline) {
            applyShowOutline(false);
        }
        super.setSpatial(spatial);

        if (spatial == null) {
            body = null;
            return;
        }
        body = spatial.getControl(RigidBodyControl.class);
        if (body == null) {
            throw new IllegalStateException("No RigidBodyControl");
        }

        if (this.spatial != null) {
            applyShowOutline(showOutline);
        }
    }

    public IngameAppstate getMain() {
        return main;
    }

    public PhysicsSpace getPhysics() {
        return physics;
    }

    public Quaternion getRotation() {
        body.getPhysicsRotation(rotation);
        return rotation;
    }

    public Vector3f getVelocity() {
        return body.getLinearVelocity();
    }

    public Vector3f getLocation() {
        return spatial.getWorldTranslation();
    }

    @Override
    protected void controlUpdate(float tpf) {
        body.applyCentralForce(acceleration.mult(tpf));
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
    }

    /**
     * Called before the physics is actually stepped, use to apply forces etc.
     *
     * @param space the physics space
     * @param tpf the time per frame in seconds
     */
    public void prePhysicsTick(PhysicsSpace space, float tpf) {
    }

    /**
     * Called after the physics has been stepped, use to check for forces etc.
     *
     * @param space the physics space
     * @param tpf the time per frame in seconds
     */
    public void physicsTick(PhysicsSpace space, float tpf) {
    }

    /**
     * Called when a collision happened in the PhysicsSpace, <i>called from
     * render thread</i>.
     *
     * Do not store the event object as it will be cleared after the method has
     * finished.
     *
     * @param event the CollisionEvent
     */
    public void collisionAny(PhysicsCollisionEvent event) {
        // if we are involved, forward it on.
        if (event.getObjectA() == body || event.getObjectB() == body) {
            collision(event);
        }
    }

    /**
     * Called when a collision happened in the PhysicsSpace involving this
     * object, <i>called from render thread</i>.
     *
     * Do not store the event object as it will be cleared after the method has
     * finished.
     *
     * @param event the CollisionEvent
     */
    public void collision(PhysicsCollisionEvent event) {
    }

    /**
     * Removes this object from the physics world
     */
    public void disablePhysics() {
        body.getPhysicsSpace().remove(spatial);
    }

    public boolean isShowOutline() {
        return showOutline;
    }

    public void setShowOutline(boolean showOutline) {
        if (showOutline == this.showOutline) {
            return;
        }
        this.showOutline = showOutline;
        if (spatial == null) {
            return;
        }
        if (!(this.spatial instanceof Node)) {
            return;
        }
        applyShowOutline(showOutline);
    }

    private void applyShowOutline(boolean showOutline) {
        if (showOutline) {
            Quad q = new Quad(10, 10);
            Geometry geom = new Geometry("Marker", q);
            if (markerMat == null) {
                markerMat = new Material(main.getAssetManager(),
                        "Common/MatDefs/Misc/Unshaded.j3md");
                markerMat.setTexture("ColorMap", main.getAssetManager().loadTexture("Textures/Highlight.png"));
                markerMat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
                markerMat.getAdditionalRenderState().setDepthTest(false);
            }
            geom.setMaterial(markerMat);
            geom.setQueueBucket(RenderQueue.Bucket.Transparent);
            geom.setLocalTranslation(-5, -5, 0);

            Node highlight = new Node(TAG_NAME);
            highlight.attachChild(geom);

            BillboardControl control = new BillboardControl();
            control.setAlignment(BillboardControl.Alignment.Screen);
            highlight.addControl(control);
            ((Node) this.spatial).attachChild(highlight);
        } else {
            ((Node) this.spatial).detachChildNamed(TAG_NAME);
        }
    }

}
