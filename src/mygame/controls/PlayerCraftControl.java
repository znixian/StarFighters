/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.controls;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Line;
import com.jme3.math.Plane;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import mygame.states.IngameAppstate;
import mygame.weapons.WeaponMissileSingleShot;
import mygame.weapons.WeaponSlot;

/**
 *
 * @author znix
 */
public class PlayerCraftControl extends CraftControl {

    public static Spatial addCraft(IngameAppstate main, PhysicsSpace physics) {
        Spatial craft = main.getAssetManager().loadModel("Models/Craft1BP.j3o");
        RigidBodyControl body;
        craft.addControl(body = new RigidBodyControl(2));
        craft.addControl(new PlayerCraftControl(main, physics));

        body.setDamping(0.05f, 0.1f);

        physics.add(craft);
        return craft;
    }

    private boolean fwd, bck, lft, rgt, rol, ror, vup, vdw, trk;
    private float hor, vtc;
    private float rolM, ptcM;
    private final Vector3f force = new Vector3f();
    private final Vector3f tmp1 = new Vector3f();
    private final Vector3f tmp2 = new Vector3f();
    private final Quaternion tmp = new Quaternion();
    private final AssetManager assetManager;
    private final Quaternion rotationCurrent = new Quaternion();
    private Spatial target;

    public PlayerCraftControl(IngameAppstate main, PhysicsSpace physics) {
        super(main, physics);
        this.assetManager = main.getAssetManager();
        weapons = new WeaponSlot[]{
            new WeaponSlot(new WeaponMissileSingleShot(WeaponMissileSingleShot.MissileType.KINETIC)),
            new WeaponSlot(new WeaponMissileSingleShot(WeaponMissileSingleShot.MissileType.EXPLOSIVE))
        };
        setShowOutline(false);
    }

    public void setFwd(boolean fwd) {
        this.fwd = fwd;
    }

    public void setBck(boolean bck) {
        this.bck = bck;
    }

    public void setLft(boolean lft) {
        this.lft = lft;
    }

    public void setRgt(boolean rgt) {
        this.rgt = rgt;
    }

    public void setRol(boolean rol) {
        this.rol = rol;
    }

    public void setRor(boolean ror) {
        this.ror = ror;
    }

    public void addHor(float hor) {
        this.hor += hor;
    }

    public void addVtc(float vtc) {
        this.vtc += vtc;
    }

    public void setVup(boolean vup) {
        this.vup = vup;
    }

    public void setVdw(boolean vdw) {
        this.vdw = vdw;
    }

    public void setTrk(boolean trk) {
        this.trk = trk;
    }

    // getters
    public boolean isTrk() {
        return trk;
    }

    @Override
    public void prePhysicsTick(PhysicsSpace space, float tpf) {
        // rotational motion
        if (trk) {
            if (target != null) {
                tmp1.set(getLocation());
                tmp2.set(target.getWorldTranslation());
                tmp2.subtractLocal(tmp1);
                tmp2.normalizeLocal();

                body.getPhysicsRotation(rotationCurrent);

                rotationCurrent.mult(Vector3f.UNIT_Y, tmp1);
                tmp.lookAt(tmp2, tmp1);
                tmp.multLocal(0, -1, 0, 1);
                rotationCurrent.slerp(tmp, tpf * 2);
//                rotationCurrent.set(tmp);

                body.setPhysicsRotation(rotationCurrent);
            }
            force.set(0, 0, 0);
        } else {
            force.set(hor, 0, vtc);
            hor -= force.x;
            vtc -= force.z;
        }

        if (ror && rol) {
        } else if (rol) {
            force.addLocal(0, -0.025f, 0);
        } else if (ror) {
            force.addLocal(0, 0.025f, 0);
        }

        force.multLocal(tpf * body.getMass() * 20f);

        body.getPhysicsRotation(rotationCurrent);
        tmp.fromAngleAxis(force.y, Vector3f.UNIT_X);
        rotationCurrent.multLocal(tmp);
        tmp.fromAngleAxis(-force.x, Vector3f.UNIT_Y);
        rotationCurrent.multLocal(tmp);
        tmp.fromAngleAxis(force.z, Vector3f.UNIT_Z);
        rotationCurrent.multLocal(tmp);
        body.setPhysicsRotation(rotationCurrent);

        // translational motion
        force.set(0, 0, 0);
        if (fwd) {
            force.addLocal(1, 0, 0);
        } else if (bck) {
            force.addLocal(-0.5f, 0, 0);
        }
        if (lft && rgt) {
        } else if (lft) {
            force.addLocal(0, 0, -0.5f);
        } else if (rgt) {
            force.addLocal(0, 0, 0.5f);
        }
        if (vup && vdw) {
        } else if (vup) {
            force.addLocal(0, 0.5f, 0);
        } else if (vdw) {
            force.addLocal(0, -0.5f, 0);
        }

        force.multLocal(tpf * body.getMass() * 1000);
        rotationCurrent.mult(force, force);
        body.applyCentralForce(force);
    }

    public void shoot() {
        WeaponSlot weapon = getFirstReadyWeapon();
        if (weapon != null) {
            weapon.fire(this);
        }
    }

    public void target() {
        if (target != null) {
            PhysicalObjectControl control = target.getControl(PhysicalObjectControl.class);
            if (control != null) {
                control.setShowOutline(true);
            }
        }

        Plane plane = new Plane();

        tmp1.set(1, 0, 0);
        tmp2.set(getLocation());
        rotationCurrent.mult(tmp1, tmp1);
        plane.setNormal(tmp1);
        tmp1.normalizeLocal();
        Line line = new Line(tmp2.clone(), tmp1.clone());

        float best = -1;
        Spatial bestSpat = null;

        for (Spatial spat : getSpatial().getParent().getChildren()) {
            if (spat == spatial) { // it's us
                continue;
            }

            tmp1.set(spat.getLocalTranslation());
            float fromCenter = line.distance(tmp1);
            tmp1.subtractLocal(tmp2);
            if (plane.whichSide(tmp1) != Plane.Side.Positive) {
                continue;
            }
            float forwards = plane.pseudoDistance(tmp1);

            float distance = fromCenter / forwards;

            if (distance < best || bestSpat == null) {
                best = distance;
                bestSpat = spat;
            }
        }

        target = bestSpat;

        if (target != null) {
            PhysicalObjectControl control = target.getControl(PhysicalObjectControl.class);
            if (control != null) {
                control.setShowOutline(false);
            }
        }
    }

    @Override
    public Spatial getCurrentTarget() {
        return target;
    }
}
