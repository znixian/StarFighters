/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.controls;

import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import mygame.payloads.Payload;
import mygame.states.IngameAppstate;

/**
 *
 * @author znix
 */
public class MissileControl extends PhysicalObjectControl {

    public static final float MIN_CORRECTION_DIST = 0.001f;
    private final Vector3f fixedDir = new Vector3f();
    private final Vector3f targetDir = new Vector3f();
    private final Payload payload;
    private boolean readyToDetonate, primed;
    private float distanceLastSq;
    private float distanceNowSq;
    private float aiTimer = 0.75f;

    protected Spatial target;
    protected boolean initPoint = false; //true;

    public static Spatial addMissile(IngameAppstate main,
            PhysicsSpace physics, Payload payload, CraftControl parent,
            Quaternion offsetRot, Vector3f offsetPos) {
        Spatial missile = addMissile(main, physics, payload);
        RigidBodyControl body = missile.getControl(RigidBodyControl.class);
        body.setPhysicsLocation(parent.getLocation().add(offsetPos));
        body.setLinearVelocity(parent.getVelocity());
        body.setPhysicsRotation(parent.getRotation().mult(offsetRot));
        return missile;
    }

    public static Spatial addMissile(IngameAppstate main,
            PhysicsSpace physics, Payload payload) {
        Spatial missile = main.getAssetManager().loadModel("Models/Missile1BP.j3o");
//        Material mat_default = new Material(
//                main.getAssetManager(), "Common/MatDefs/Misc/ShowNormals.j3md");
//        missile.setMaterial(mat_default);
//        missile.setLocalScale(0.5f);

        RigidBodyControl body;
        missile.addControl(body = new RigidBodyControl(2));
        MissileControl control;
        missile.addControl(control = new MissileControl(payload, main, physics));

        physics.add(missile);

        body.setDamping(0.35f, 0.1f);

        return missile;
    }

    public MissileControl(Payload payload, IngameAppstate main, PhysicsSpace physicsSpace) {
        super(main, physicsSpace);
        this.payload = payload;
    }

    @Override
    protected void controlUpdate(float tpf) {
        aiTimer -= tpf;
        if (readyToDetonate) {
            primed = true;
            if (distanceNowSq > distanceLastSq) {
                // moving further away
                payload.detonate(this);
            }
        } else {
//            if (primed) {
//                // leaving the detonation zone, better to physically hit it
//                payload.detonate(this);
//            }
            primed = false;
        }
    }

    public Spatial getTarget() {
        return target;
    }

    public void setTarget(Spatial target) {
        this.target = target;
    }

    @Override
    public void prePhysicsTick(PhysicsSpace space, float tpf) {
        Spatial tg = getTarget();
        readyToDetonate = false;
        if (tg != null && aiTimer < 0) {
            targetDir.set(tg.getLocalTranslation());
            targetDir.subtractLocal(spatial.getLocalTranslation());

            float range = payload.getMaxDetonateRange();
            float distSq = targetDir.lengthSquared();
            if (distSq < range * range) {
                readyToDetonate = true;
//                return;
            }
            distanceLastSq = distanceNowSq;
            distanceNowSq = distSq;

            targetDir.normalizeLocal();

            Vector3f prograde = body.getLinearVelocity().normalizeLocal();

            // compensate for overshooting target
            prograde.subtract(targetDir, fixedDir);
            fixedDir.normalizeLocal();
            if (fixedDir.lengthSquared() < MIN_CORRECTION_DIST * MIN_CORRECTION_DIST) {
                targetDir.set(fixedDir);
            }

            Quaternion dir = new Quaternion();
            dir.lookAt(targetDir, Vector3f.UNIT_Y);
            dir.multLocal(0, -1, 0, 1);
//            dir.lookAt(prograde, Vector3f.UNIT_Y);

            Quaternion newRotation = new Quaternion();
            Quaternion currentRotation = body.getPhysicsRotation();
            newRotation.slerp(currentRotation, dir, tpf * 10);
//            newRotation.set(dir);

            if (initPoint) {
                body.setPhysicsRotation(dir);
            } else {
                body.setPhysicsRotation(newRotation);
            }
            initPoint = false;
        }

        float thrust = 20000; // 10000;

        Vector3f facingNow = body.getPhysicsRotation().mult(Vector3f.UNIT_X);

        Vector3f engineThrust = facingNow.clone();
        engineThrust.normalizeLocal();
//        engineThrust.negateLocal(); // we want to hit it in the opposite direction
        engineThrust.multLocal(tpf * thrust);
        body.applyCentralForce(engineThrust);
    }

    @Override
    public void collision(PhysicsCollisionEvent event) {
        payload.collide(this, event);
    }

    public void destroy() {
        spatial.removeFromParent();
        disablePhysics();
    }

    public RigidBodyControl getBody() {
        return body;
    }
}
