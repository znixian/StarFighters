/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.controls;

import com.jme3.bullet.PhysicsSpace;
import com.jme3.math.Quaternion;
import com.jme3.scene.Spatial;
import mygame.states.IngameAppstate;
import mygame.weapons.WeaponSlot;

/**
 *
 * @author znix
 */
public abstract class CraftControl extends PhysicalObjectControl {

    protected WeaponSlot[] weapons;

    public CraftControl(IngameAppstate main, PhysicsSpace physics) {
        super(main, physics);
        acceleration.set(0, 0, 0);
    }

    @Override
    protected void controlUpdate(float tpf) {
        super.controlUpdate(tpf);
        if (weapons != null) {
            for (WeaponSlot weapon : weapons) {
                weapon.update(tpf);
            }
        }
    }

    protected WeaponSlot getFirstReadyWeapon() {
        if (weapons != null) {
            for (WeaponSlot weapon : weapons) {
                if (weapon.isReady()) {
                    return weapon;
                }
            }
        }
        return null;
    }

    public Spatial getCurrentTarget() {
        return null;
    }

    public Quaternion getRotationOffset() {
        return Quaternion.IDENTITY;
    }
}
