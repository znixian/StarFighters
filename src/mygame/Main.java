package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.renderer.RenderManager;
import com.jme3.system.AppSettings;
import mygame.states.IngameAppstate;
import org.lwjgl.LWJGLException;

/**
 * This is the Main Class of your Game. You should only do initialization here.
 * Move your Logic into AppStates or Controls
 *
 * @author normenhansen
 */
public class Main extends SimpleApplication {

    private IngameAppstate ingameState;

    public static void main(String[] args) throws LWJGLException {
        InjectNatives.inject();
        Main app = new Main();
        AppSettings settings = new AppSettings(true);
        if (!SettingsDialog.showSettingsDialog(settings, true)) {
            return;
        }
        app.setSettings(settings);
        app.setShowSettings(false);
        app.start();
    }

    @Override
    public void simpleInitApp() {
        flyCam.setMoveSpeed(250);
        cam.setFrustumFar(1000000);

        ingameState = new IngameAppstate();
        ingameState.initialize(stateManager, this);
        stateManager.attach(ingameState);
    }

    @Override
    public void simpleUpdate(float tpf) {
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }

    public AppSettings getSettings() {
        return settings;
    }
}
