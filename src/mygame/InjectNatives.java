/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.system.NativeLibraryLoader;
import com.jme3.system.Platform;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author znix
 */
public class InjectNatives {

    public static void inject() {
        try {
            File libs = new File("natives");
            if (!libs.exists()) {
                libs.mkdirs();
                extractNatives(libs);
            }
            addLibraryPath(libs.getAbsolutePath());
        } catch (IllegalArgumentException | IllegalAccessException |
                IOException | NoSuchFieldException ex) {
            Logger.getLogger(InjectNatives.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void extractNatives(File folder) throws IOException {
        NativeLibraryLoader.extractNativeLibraries(Platform.Windows32, folder);
        NativeLibraryLoader.extractNativeLibraries(Platform.Windows64, folder);
        NativeLibraryLoader.extractNativeLibraries(Platform.Linux32, folder);
        NativeLibraryLoader.extractNativeLibraries(Platform.Linux64, folder);
        NativeLibraryLoader.extractNativeLibraries(Platform.MacOSX32, folder);
        NativeLibraryLoader.extractNativeLibraries(Platform.MacOSX64, folder);
    }

    private static void addLibraryPath(String pathToAdd) throws
            NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        final Field usrPathsField = ClassLoader.class.getDeclaredField("usr_paths");
        usrPathsField.setAccessible(true);

        //get array of paths
        final String[] paths = (String[]) usrPathsField.get(null);

        //check if the path to add is already present
        for (String path : paths) {
            if (path.equals(pathToAdd)) {
                return;
            }
        }

        //add the new path
        final String[] newPaths = Arrays.copyOf(paths, paths.length + 1);
        newPaths[newPaths.length - 1] = pathToAdd;
        usrPathsField.set(null, newPaths);
    }
}
